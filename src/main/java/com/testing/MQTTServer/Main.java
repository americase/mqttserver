package com.testing.MQTTServer;

import com.home.mqtt.subscriber.Subscriber;
import com.home.mqtt.subscriber.TempSub;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.eclipse.paho.client.mqttv3.MqttException;

/**
 *
 * @author abdis
 */
public class Main {

    public static String BROKER_ADDRESS = "tcp://192.168.1.55:1883";
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        try {
            db.DBManager.getConnection();
            Thread lightSubscriber = new Thread(new SubscriberThread(new Subscriber("home/sensor/light")));
            Thread tempSubscriber = new Thread(new SubscriberThread(new Subscriber("home/sensor/temp")));
            Thread electricitySubscriber = new Thread(new SubscriberThread(new Subscriber("home/sensor/electricity")));
            
            lightSubscriber.start();
            tempSubscriber.start();
            electricitySubscriber.start();
            
        } catch (MqttException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
