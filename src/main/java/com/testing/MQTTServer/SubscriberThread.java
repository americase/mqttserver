/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.testing.MQTTServer;

import com.home.mqtt.subscriber.Subscriber;

/**
 *
 * @author abdis
 */
public class SubscriberThread implements Runnable{
    
    private final Subscriber subscriber;

    public SubscriberThread(Subscriber subscriber) {
        this.subscriber = subscriber;
    }

    @Override
    public void run() {
        subscriber.subscribe();
    }
    
}
