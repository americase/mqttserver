 package com.testing.MQTTServer;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import db.MessageTable;
import java.sql.SQLException;

/**
 *
 * @author abdis
 */
public class MQTT{

    public void saveMessage(String JSONString) {
        Gson gson = new GsonBuilder().setPrettyPrinting().create();

        Message message = gson.fromJson(JSONString, Message.class);
        try {
            MessageTable.insertMessage(message);
            System.err.println("Databased");
        } catch (SQLException ex) {
            System.err.println("Error while databasing\n"+ex.getMessage());            
        }
    }

}
