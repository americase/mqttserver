package com.home.mqtt.subscriber;

import com.testing.MQTTServer.MQTT;
import com.testing.MQTTServer.Main;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.eclipse.paho.client.mqttv3.IMqttClient;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;

/**
 *
 * @author abdis
 */
public class Subscriber{

    protected final String uuid;
    protected final String topic;
    protected final IMqttClient client;
    
    public Subscriber(String topic) throws MqttException{
        this.uuid = UUID.randomUUID().toString();
        this.client = new MqttClient(Main.BROKER_ADDRESS, uuid);
        this.topic = topic;
        client.connect();
    }

    public String getUuid() {
        return uuid;
    }

    public String getTopic() {
        return topic;
    }

    public IMqttClient getClient() {
        return client;
    }
    
    public void subscribe(){
        if (!client.isConnected()) {
            System.out.println("Connect client");
        }
        client.setCallback(new MqttCallback() {
            @Override
            public void connectionLost(Throwable thrwbl) {
                System.out.println("Connection lost");
                thrwbl.printStackTrace();
            }

            @Override
            public void messageArrived(String string, MqttMessage mm) throws Exception {
                MQTT mqtt = new MQTT();
                mqtt.saveMessage(mm.toString());
            }

            @Override
            public void deliveryComplete(IMqttDeliveryToken imdt) {
                System.out.println("Done!");
            }
        });
        try {
            client.subscribe(topic);
        } catch (MqttException ex) {
            Logger.getLogger(TempSub.class.getName()).log(Level.SEVERE, null, ex);
            System.exit(0);
        }
    }
    
}
