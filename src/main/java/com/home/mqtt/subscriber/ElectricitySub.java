/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.home.mqtt.subscriber;

import com.testing.MQTTServer.MQTT;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;

/**
 *
 * @author abdis
 */
public class ElectricitySub extends Subscriber{

    public ElectricitySub() throws MqttException {
        super("home/sensor/electricty");
    }

    @Override
    public void subscribe() {
        if (!client.isConnected()) {
            System.out.println("Connect client");
        }
        client.setCallback(new MqttCallback() {
            @Override
            public void connectionLost(Throwable thrwbl) {
                System.out.println("Connection lost");
                thrwbl.printStackTrace();
            }

            @Override
            public void messageArrived(String string, MqttMessage mm) throws Exception {
                MQTT mqtt = new MQTT();
                mqtt.saveMessage(mm.toString());
            }

            @Override
            public void deliveryComplete(IMqttDeliveryToken imdt) {
                System.out.println("Done!");
            }
        });
        try {
            client.subscribe(topic);
        } catch (MqttException ex) {
            Logger.getLogger(TempSub.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
